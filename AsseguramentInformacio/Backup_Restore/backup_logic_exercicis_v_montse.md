# Backup lògic

![diagrama backup i restauració lògica amb Postgresql](https://gitlab.com/msolerprat/uf3-sql-ddl-i-dml-public/-/raw/main/AsseguramentInformacio/Backup_Restore/PostgreSQL_dump_restore.svg.png)

## Eines de PostgreSQL

Quan el sistema gestor de Base de Dades Relacional (SGBDR o RDBMS en anglès) és
PostgreSQL, les ordres clàssiques per fer la còpia de base de dades són:

+ `pg_dump` per copiar una base de dades del SGBDR a un fitxer script o de
  tipus arxiu 

+ `pg_dumpall` per copiar totes les bases de dades del SGBDR a un fitxer script
  SQL


Anàlogament, quan volem restaurar una base de dades si el fitxer destí ha estat un script SQL farem la restauració amb l'ordre `psql`, mentre que si el fitxer destí és de tipus binari l'ordre a utilitzar serà `pg_restore`.

Respon i després *executa les
instruccions comprovant empíricament que són correctes*.


## Pregunta 1. 

Quina instrucció ens fa una còpia de seguretat lògica de la base de dades training (de tipus script SQL) ?

## Pregunta 2.

El mateix d'abans però ara el destí és un directori.


## Pregunta 3.

El mateix d'abans però ara el destí és un fitxer tar.


## Pregunta 4. 

El mateix d'abans però ara el destí és un fitxer tar però la base de dades és mooolt gran i cal comprimir el fitxer tar amb gzip

I com restauraries la base de dades a partir del fitxer obtingut?

## Pregunta 5

El mateix que abans però ara el destí és un directori i a més a més es vol optimitzar el temps
d'execució. No pots esperar tant de temps en fer el backup. Quina ordre
aconseguirà treballar en paral·lel? Fes la mateixa ordre d'abans però atacant
la info de les 5 taules de la base de dades al mateix temps.


## Pregunta 6

Si no indiquem usuari ni tipus de fitxer quin és el valor per defecte?

## Pregunta 7

Fes una còpia de seguretat lògica només de la taula _comandes_ de tipus script SQL.

## Pregunta 8

Fes una còpia de seguretat lògica només de la taula _rep_vendes_ de tipus binari.

## Pregunta 9 

Elimina la taula _comandes_ de la base de dades _training_. 

Quina ordre restaura la taula _comandes_? (recorda que el fitxer és un script SQL) 

## Pregunta 10

Fes un cop d'ull al backup tar creat a l'exercici 3 (per exemple amb l'ordre `tar tvf` ). Creus que a partir d'aquest fitxer es podria restaurar només una taula? 
Si és així, escriu i comprova-ho amb rep_vendes (abans hauràs d'eliminar la taula amb `DROP`)


## Pregunta 11

Elimina la taula *rep_vendes* de la base de dades training. 

Quina ordre restaura la taula *rep_vendes* (recorda que el fitxer és binari)

## Pregunta 12

Després de fer una còpia de tota la base de dades training en format binari, elimina-la. 

Hi ha l'ordre de bash `dropdb` (en realitat és un wrapper de DROP DATABASE). I de manera anàloga també hi ha l'ordre de bash `createdb`.
Sense utilitzar aquesta última i amb una única ordre restaura la base de dades training.



## Pregunta 13

Per defecte, si n'hi ha un error quan fem la restauració `psql` ignora l'error i continuarà executant-se. Si volem que pari just quan hi hagi l'errada quina opció afegirem a l'ordre `psql` ?

## Pregunta 14

Si es vol que la restauració es faci en mode transacció, és a dir o es fa tot o no es fa res, com seria l'ordre?

## Pregunta 15

Quina ordre em permet fer un backup de **totes** les bases de dades d'un cluster? Es necessita ser l'administrador de la base de dades?

## Pregunta 16

Quina ordre em permet restaurar **totes** les bases de dades d'un cluster a partir del fitxer `backup_all_db.sql`? Es necessita ser l'administrador de la base de dades?

## Pregunta 17

Quina ordre em permet fer una còpia de seguretat de tipus `tar` la base de dades `training` que es troba a 192.168.0.123 ? Canvia la IP per la de l'ordinador del teu company.

## Pregunta 18

Es pot fer un backup de la base de dades training del server1 i que es restauri _on the fly_ a server2?


