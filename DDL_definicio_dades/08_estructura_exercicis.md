# Exercicis DDL (Estructura)

## Exercici 1

Crear una base de dades anomenada "biblioteca". Dins aquesta base de dades:
+ crear una taula anomenada llibres amb els camps següents:
	+ "ref": clau primaria numèrica que identifica els llibres i s'ha d'assignar automàticament.
	+ "titol": títol del llibre.
	+ "editorial": nom de l'editorial.
	+ "autor": identificador de l'autor, clau forana, per defecte ha de
	referenciar al primer valor de la taula autors que simbolitza l'autor "Anònim".
+ Crear una altre taula anomenada "autors" amb els següents camps:
	+ "autor": identificador de l'autor.
	+ "nom": Nom i cognoms de l'autor.
	+ "nacionalitat": Nacionalitat de l'autor.
+ Ambdues taules han de mantenir integritat referencial. Cal que si es trenca
  la integritat per delete d'autor, la clau forana del llibre apunti a
"Anònim".
+ Si es trenca la integritat per insert/update s'ha d'impedir l'alta/modificació.
+ Cal inserir l'autor "Anònim" a la taula autors.

## Exercici 2

A l'anterior BD biblioteca:
+ Afegirem una taula anomenada _socis_ amb els següents camps:
	+ num_soci: clau primària
	+ nom: nom i cognoms del soci.
	+ dni: DNI del soci.
+ Afegirem també una taula anomenada _prestecs_ amb els següents camps:
	+ ref: clau forana, que fa referència al llibre prestat.
	+ soci: clau forana, que fa referència al soci.
	+ data_prestec: data en que s'ha realitzat el préstec.
+ No cal que prestecs tingui clau principal ja que només és una taula de relació.
+ En eliminar un llibre cal que s'eliminin els seus préstecs automàticament.
+ No s'ha de poder eliminar un soci amb préstecs pendents.


## Exercici 3

A la base de dades training crear una taula anomenada "rep_vendes_baixa" que
tingui la mateixa estructura que la taula rep_vendes i a més a més un camp
anomenat "baixa" que pugui contenir la data en que un representant de vendes
s'ha donat de baixa.

## Exercici 4

A la base de dades training crear una taula anomenada
"productes_sense_comandes" omplerta amb aquells productes que no han tingut mai
cap comanda. A continuació esborrar de la taula "productes" aquells productes
que estan en aquesta nova taula.

## Exercici 5

A la base de dades training crear una taula temporal que substitueixi la taula
"clients" però només ha de contenir aquells clients que no han fet comandes i
tenen assignat un representant de vendes amb unes vendes inferiors al 110% de
la seva quota.

## Exercici 6

Escriu les sentències necessàries per a crear l'estructura de l'esquema
proporcionat de la base de dades training. Justifica les accions a realitzar en
modificar/actualitzar les claus primàries.

## Exercici 7

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "nif" a la taula "clients" que
permeti emmagatzemar el NIF de cada client. També s'ha de procurar que el NIF
de cada client sigui únic.

## Exercici 8

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que afegeixi un camp anomenat "tel" a la taula "clients" que
permeti emmagatzemar el número de telèfon de cada client. També s'ha de
procurar que aquest contingui 9 xifres.

## Exercici 9

Escriu les sentències necessàries per modificar la base de dades training
proporcionada. Cal que s'impedeixi que els noms dels representants de vendes i
els noms dels clients estiguin buits, és a dir que ni siguin nuls ni continguin
la cadena buida.

## Exercici 10

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que procuri que l'edat dels representants de vendes no sigui
inferior a 18 anys ni superior a 65.

## Exercici 11

Escriu una sentència que permeti modificar la base de dades training
proporcionada. Cal que esborri el camp "carrec" de la taula "rep_vendes"
esborrant també les possibles restriccions i referències que tingui.

## Exercici 12

Escriu les sentències necessàries per modificar la base de dades training
proporcionada per tal que aquesta tingui integritat referencial. Justifica les
accions a realitzar per modificar les dades.

